using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cority_Challenge.UnitTests
{
    /// <summary>
    /// This class is responsible for testing the Camping Expenses
    /// </summary>
    [TestClass]
    public class CampingTripTest
    {
        #region Test Methods

        /// <summary>
        /// A test for total number of persons in camp
        /// </summary>
        [TestMethod]
        public void TotalNumberOfPersonInCampTest()
        {
            var campingTrip = new CampingTrip(4);
            //assert
            Assert.AreEqual(campingTrip.TotalPersons, 4);
        }

        /// <summary>
        /// A test for Total Camp Expenses
        /// </summary>
        [TestMethod]
        public void TotalExpensesOfCampingTripTest()
        {
            var campingTrip = new CampingTrip(2);
            //Expenses of 1st person
            campingTrip.CampingExpense.Add(new CampingExpense(1, 100.00m));
            campingTrip.CampingExpense.Add(new CampingExpense(1, 200.00m));
            //Expenses of 2nd person
            campingTrip.CampingExpense.Add(new CampingExpense(2, 110.00m));
            campingTrip.CampingExpense.Add(new CampingExpense(2, 218.05m));
            /*
             * Calculation:
             * Total Expense = Sum of expenses by each persons
             *               = Total Expenses of 1st person + Total Expenses of 2nd person
             *               = 300.00 + 328.05
             * Total Expense = 628.05
             */
            Assert.AreEqual(campingTrip.TotalCampExpense, 628.05m);
        }

        /// <summary>
        /// A test for Total Amount Owed per person
        /// </summary>
        [TestMethod]
        public void TotalAmountOwedPerPersonTest()
        {
            var campingTrip = new CampingTrip(3);
            //Expenses of 1st person
            campingTrip.CampingExpense.Add(new CampingExpense(1, 100.00m));
            campingTrip.CampingExpense.Add(new CampingExpense(1, 200.00m));
            //Expenses of 2nd person
            campingTrip.CampingExpense.Add(new CampingExpense(2, 1110.00m));
            campingTrip.CampingExpense.Add(new CampingExpense(2, 218.05m));
            //Expenses of 2nd person
            campingTrip.CampingExpense.Add(new CampingExpense(3, 10.00m));
            campingTrip.CampingExpense.Add(new CampingExpense(3, 8.05m));
            /*
             * Calculation:
             * Total Expenses - 1646.1
             * Avg expenses - 548.7 per person
             * Total Expense by 2 nd person - 1328.05
             * Total Amt owed to 2ns person = Avg Expense - his expense
             *                              = 548.7 - 1328.05
             *                     Amt Owed = -779.35 
             */
            Assert.AreEqual(campingTrip.CalculateAmtOwedPerPerson(1), 248.70m);
            Assert.AreEqual(campingTrip.CalculateAmtOwedPerPerson(2), -779.35m);
            Assert.AreEqual(campingTrip.CalculateAmtOwedPerPerson(3), 530.65m);
        }

        #endregion
    }
}
