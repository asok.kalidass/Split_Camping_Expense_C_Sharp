﻿namespace Cority_Challenge
{
    /// <summary>
    /// This class is responsible for hadling Camping Expenses
    /// </summary>
    public class CampingExpense
    {
        #region Class Variables

        //Private Variables
        private decimal _camperExpense;
        private int _camperId;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialises the Camping Expense
        /// </summary>
        /// <param name="camperId"></param>
        /// <param name="camperExpense"></param>
        public CampingExpense(int camperId, decimal camperExpense)
        {
            this._camperId = camperId;
            this._camperExpense = camperExpense;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or Sets CamperExpense
        /// </summary>
        public decimal CamperExpense
        {
            get
            {
                return _camperExpense;
            }
            set
            {
                _camperExpense = value;
            }
        }

        /// <summary>
        /// Gets or Sets CamperId
        /// </summary>
        public int CamperId
        {
            get
            {
                return _camperId;
            }
            set
            {
                _camperId = value;
            }
        }

        #endregion
    }
}
