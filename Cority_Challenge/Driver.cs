﻿using System;
using System.IO;

namespace Cority_Challenge
{
    /// <summary>
    /// Thsi class is the Main driver class
    /// </summary>
    class Driver
    {
        /// <summary>
        /// Main Method to calculate the amount owed.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            CampingExpenseProcessor expenseProcessor = new CampingExpenseProcessor();
            try
            {               
                Console.WriteLine("Enter File Name");
                //Gets the file name from user
                var file = Console.ReadLine();
                if (!(string.IsNullOrEmpty(file)))
                {
                    if (!File.Exists(file))
                        Console.WriteLine("File Not Found");
                    //reads the file contents and calculates the amount owed
                    expenseProcessor.ReadFileContents(file);
                }
                else
                {
                    Console.WriteLine("File Name is not entered");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Some Problem Encounted." + ex.Message);               
            }
        }
    }
}
