﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cority_Challenge
{
    /// <summary>
    /// Camping Trip Class is responsible for holding total camping information
    /// </summary>
    public class CampingTrip
    {
        #region Class Variables

        //private variables
        private int _totalPersons;
        private IList<CampingExpense> _campingExpense;

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialises the Camping Trip
        /// </summary>
        /// <param name="totalPerson"></param>
        public CampingTrip(int totalPerson)
        {
            this._totalPersons = totalPerson;
            this._campingExpense = new List<CampingExpense>();
        }

        /// <summary>
        /// Calculates Total Amount Owed per Person
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public decimal CalculateAmtOwedPerPerson(int personId)
        {
            return Math.Round(((TotalTripExpense() / TotalPersons) - TotalExpensePerPerson(personId)), 2, MidpointRounding.AwayFromZero);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculates Total Trip Expenses
        /// </summary>
        /// <returns></returns>
        private decimal TotalTripExpense()
        {
            return CampingExpense.Sum(x => x.CamperExpense);
        }

        /// <summary>
        /// Calculates Total Trip Expense by Person
        /// </summary>
        /// <param name="personId">person</param>
        /// <returns></returns>
        private decimal TotalExpensePerPerson(int personId)
        {
            return CampingExpense.Where(x => x.CamperId.Equals(personId)).Sum(y => y.CamperExpense);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or Sets CampingExpense
        /// </summary>
        public IList<CampingExpense> CampingExpense
        {
            get
            {
                return _campingExpense;
            }
            set
            {
                _campingExpense = value;
            }
        }

        /// <summary>
        /// Gets or Sets TotalPersons
        /// </summary>
        public int TotalPersons
        {
            get
            {
                return _totalPersons;
            }
            set
            {
                _totalPersons = value;
            }
        }

        /// <summary>
        /// Gets TotalCampExpense
        /// </summary>
        public decimal TotalCampExpense
        {
            get
            {
                return TotalTripExpense();
            }
        }

        #endregion
    }
}
