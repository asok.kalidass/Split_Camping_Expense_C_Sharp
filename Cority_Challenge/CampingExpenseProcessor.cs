﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Cority_Challenge
{
    /// <summary>
    /// This class is responsible for file processing and calculating the exspenses
    /// </summary>
    public class CampingExpenseProcessor
    {
        #region Class Variables

        //Private variables
        IList<CampingTrip> _campingTrips; //hold the camping info of each trip
        int _count = 0; //used to iterate over the file contents

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialises the Camping Expense Processor
        /// </summary>
        public CampingExpenseProcessor()
        {
            this._campingTrips = new List<CampingTrip>();
        }

        /// <summary>
        /// Processes the file to calculate the expense
        /// </summary>
        /// <param name="fileName">input file</param>
        public void ReadFileContents(string fileName)
        {
            var fileContents = File.ReadAllLines(fileName);

            if (fileContents.Length < 1)
                Console.WriteLine("Empty File");

            while (Convert.ToInt16(fileContents[_count]) != 0)
                PopulateCampingInfo(fileContents);

            SplitUpExpenses(fileName);

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Splits up the owed amount and writes to the output file
        /// </summary>
        /// <param name="fileName">input file</param>
        private void SplitUpExpenses(string fileName)
        {
            using (TextWriter writer = new StreamWriter(fileName + ".out"))
            {
                //calculate the owed amount of each person per camping trip
                foreach (CampingTrip trip in _campingTrips)
                {
                    for (int person = 1; person <= trip.TotalPersons; person++)
                    {
                        decimal owedAmount = trip.CalculateAmtOwedPerPerson(person);
                        writer.WriteLine(owedAmount > 0 ? string.Format("${0}", owedAmount.ToString()) : string.Format("(${0})", Math.Abs(owedAmount).ToString()));
                    }
                    writer.WriteLine(" ");
                }
            }
        }

        /// <summary>
        /// Populates the Camping info
        /// </summary>
        /// <param name="fileContents">Camping Info</param>
        private void PopulateCampingInfo(string[] fileContents)
        {
            //create the dummy camping trip with the number of person and empty expenses
            var campingTrip = new CampingTrip(Convert.ToInt16(fileContents[_count++]));
            PopulateCampingExpenseInfo(fileContents, campingTrip);
        }

        /// <summary>
        /// Populates the Camping Expenses
        /// </summary>
        /// <param name="fileContents">file contents</param>
        /// <param name="campingTrip">Camping Info</param>
        private void PopulateCampingExpenseInfo(string[] fileContents, CampingTrip campingTrip)
        {
            //for each person add the camping expenses
            for (int person = 1; person <= campingTrip.TotalPersons; person++)
            {
                int noOfPersonExpenses = Convert.ToInt16(fileContents[_count++]);

                for (int personExpenses = 1; personExpenses <= noOfPersonExpenses; personExpenses++)
                {
                    campingTrip.CampingExpense.Add(new CampingExpense(person, Convert.ToDecimal(fileContents[_count++])));
                }
            }

            _campingTrips.Add(campingTrip);
        }

        #endregion
    }
}
